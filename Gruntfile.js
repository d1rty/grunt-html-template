/*
 * grunt-html-template
 * 
 *
 * Copyright (c) 2016 Tristan Cebulla
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        dir: {
            src: 'foo'
        },
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: [
                'Gruntfile.js',
                'tasks/*.js',
                '<%= nodeunit.tests %>'
            ],
            options: {
                jshintrc: '.jshintrc'
            }
        },

        // Before generating any new files, remove any previously-created files.
        clean: {
            tests: ['tmp']
        },

        // Configuration to be run (and then tested).
        html_template: {
            /*
             default_options: {
             options: {
             },
             files: {
             'tmp/default_options': ['test/fixtures/testing', 'test/fixtures/123']
             }
             },
             custom_options: {
             options: {
             separator: ': ',
             punctuation: ' !!!'
             },
             files: {
             'tmp/custom_options': ['test/fixtures/testing', 'test/fixtures/123']
             }
             },

             */
            default_options: {
                options: {
                    removeComments: true
                },
                files: {
                    'tmp/more.html': ['test/fixtures/grunt-html-template.html'],
//          'tmp/<%= pkg.name %>.html': [ 'test/fixtures/testing' ]
//            'tmp/<%= pkg.name %>.html2': 'test/fixtures/<%= pkg.name %>.html'
                },
                replace: {
                    foo: 'Bar'
                }
            }

        },

        // Unit tests.
        nodeunit: {
            tests: ['test/*_test.js']
        }

    });

    // Actually load this plugin's task(s).
    grunt.loadTasks('tasks');

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-nodeunit');

    // Whenever the "test" task is run, first clean the "tmp" dir, then run this
    // plugin's task(s), then test the result.
    grunt.registerTask('test', ['clean', 'html_template', 'nodeunit']);

    // By default, lint and run all tests.
    grunt.registerTask('default', ['jshint', 'test']);

};
