'use strict';

var grunt = require('grunt');

/*
 ======== A Handy Little Nodeunit Reference ========
 https://github.com/caolan/nodeunit

 Test methods:
 test.expect(numAssertions)
 test.done()
 Test assertions:
 test.ok(value, [message])
 test.equal(actual, expected, [message])
 test.notEqual(actual, expected, [message])
 test.deepEqual(actual, expected, [message])
 test.notDeepEqual(actual, expected, [message])
 test.strictEqual(actual, expected, [message])
 test.notStrictEqual(actual, expected, [message])
 test.throws(block, [error], [message])
 test.doesNotThrow(block, [error], [message])
 test.ifError(value)
 */

var builder = new RegexBuilder();

exports.regex_builder = {
    setUp: function (done) {

        // setup here if necessary
        done();
    },
    simple_regex: function(test) {
        test.expect(1);

        var name = 'test-regex',
            regex = /.+/,
            patterns = [
                {
                    regex: ".+"
                }
            ];
        builder.register(name, patterns);

        test.equal(builder.build(name, 'none').toString(), regex.toString());

        test.done();
    },
    extended_regex: function (test) {


    }
};

