'use strict';

var grunt = require('grunt');

/*
 ======== A Handy Little Nodeunit Reference ========
 https://github.com/caolan/nodeunit

 Test methods:
 test.expect(numAssertions)
 test.done()
 Test assertions:
 test.ok(value, [message])
 test.equal(actual, expected, [message])
 test.notEqual(actual, expected, [message])
 test.deepEqual(actual, expected, [message])
 test.notDeepEqual(actual, expected, [message])
 test.strictEqual(actual, expected, [message])
 test.notStrictEqual(actual, expected, [message])
 test.throws(block, [error], [message])
 test.doesNotThrow(block, [error], [message])
 test.ifError(value)
 */

exports.cache = {
    setUp: function (done) {
        // setup here if necessary
        done();
    },
    simple: function(test) {
        test.expect(2);

        var cache = new Cache(),
            id = 'testdata',
            data = [
                {
                    foo: 'bar'
                }
            ];
        cache.set(id, data);

        test.equal(cache.has(id), true, 'Testing Cache.has()');
        test.equal(cache.get(id), data, 'Testing Cache.get()');

        test.done();
    }
};
