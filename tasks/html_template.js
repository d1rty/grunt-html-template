/*
 * grunt-html-template
 * 
 *
 * Copyright (c) 2016 Tristan Cebulla
 * Licensed under the MIT license.
 */

'use strict';

var fs = require('fs');

function read(file) {
    return fs.readFileSync( file ).toString();
}

function include(file) {
    eval.apply( global, [ read( file ) ] );
}

include(__dirname + '/../lib/utils.js');
include(__dirname + '/../lib/Cache.js');
include(__dirname + '/../lib/RegexBuilder.js');
include(__dirname + '/../lib/TemplateEngine.js');

module.exports = function (grunt) {
    grunt.registerMultiTask('html_template', 'Minimal template engine to create HTML files from templates', function () {

        var options = this.options(),
            replace_data = this.data.replace || {};

        // Iterate over all specified file groups.
        this.files.forEach(function (file) {

            var src = file.src.filter(function (filepath) {
                // Warn on and remove invalid source files (if nonull was set).
                if (!grunt.file.exists(filepath)) {
                    grunt.log.error('Source file "' + filepath + '" not found.');
                    return false;
                } else {
                    return true;
                }
            }).map(function (filepath) {
                return filepath;
            });

            var engine = new TemplateEngine(grunt, options);

            file.src.forEach(function (sourceFile) {
                engine.addSource(sourceFile);
            });

            engine.replaceData(replace_data);

            grunt.file.write(file.dest, engine.getTemplateContent());
            grunt.log.writeln('File "' + file.dest + '" created.');
        }); // forEach

    }); // grunt

}; // exports