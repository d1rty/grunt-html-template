/**
 * Created by equinox on 28.07.16.
 */

function extend(first, second) {
    for (var secondProp in second) {
        var secondVal = second[secondProp];
        // Is this value an object?  If so, iterate over its properties, copying them over
        if (secondVal && Object.prototype.toString.call(secondVal) === "[object Object]") {
            first[secondProp] = first[secondProp] || {};
            extend(first[secondProp], secondVal);
        }
        else {
            first[secondProp] = secondVal;
        }
    }
    return first;
}

function indexOf(array, valToFind) {
    var foundIndex = -1;
    for (var index = 0; index < array.length; index++) {
        if (array[index] === valToFind) {
            foundIndex = index;
            break;
        }
    }
    return foundIndex;
}

function keys(myObject) {
    var keys = [];
    for (var prop in myObject) {
        if (myObject.hasOwnProperty(prop)) {
            keys.push(prop);
        }
    }
    return keys;
}

function sprintf() {
    var args = arguments,
        string = args[0],
        i = 1;
    return string.replace(/%((%)|s|d)/g, function (m) {
        // m is the matched format, e.g. %s, %d
        var val = null;
        if (m[2]) {
            val = m[2];
        } else {
            val = args[i];
            // A switch statement so that the formatter can be extended. Default is %s
            switch (m) {
                case '%d':
                    val = parseFloat(val);
                    if (isNaN(val)) {
                        val = 0;
                    }
                    break;
            }
            i++;
        }
        return val;
    });
}