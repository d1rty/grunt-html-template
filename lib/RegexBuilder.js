/**
 * Created by equinox on 28.07.16.
 */

function RegexBuilder() {
    this.cache = new Cache();
    this.patterns = {};
}

RegexBuilder.prototype.register = function(name, patterns, modifier) {
    this.patterns[name] = {
        patterns: patterns,
        modifier: modifier
    };
};

RegexBuilder.prototype.escapeRegExp = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

RegexBuilder.prototype.getApplicableRegexes = function () {
    return keys(this.patterns);
};

RegexBuilder.prototype.build = function (regex_name, capture_type, variable) {
    var that = this;

    // preliminary checks
    if (regex_name == null || regex_name == '') {
        throw 'No regex name given';
    }

    if (this.patterns[regex_name] == null) {
        throw 'Unknown regex ' + regex_name;
    }

    if (
        Object.prototype.toString.call( this.patterns[regex_name].patterns ) !== '[object Array]' ||
        this.patterns[regex_name].patterns.length == 0
    ) {
        throw 'No patterns defined for ' + regex_name;
    }

    if (capture_type == null || capture_type == '') {
        capture_type = 'all';
    }

    if (indexOf( ['inner', 'all', 'none' ], capture_type) == -1) {
        throw 'Unknown capture type ' + capture_type;
    }

    // cache
    var cacheID = sprintf('%s|%s|%s', regex_name, capture_type, (variable == null) ? 'NO_VARIABLE' : variable.toString());
    if (this.cache.has(cacheID)) {
        return this.cache.get(cacheID);
    }

    // begin building the regular expression
    var string = '';

    if (capture_type == 'all') {
        string += '(';
    }

    this.patterns[regex_name].patterns.forEach(function(element) {
        element = extend({
            capture: false,
            variable: false
        }, element);

        if (capture_type == 'inner' && element.capture) {
            string += '(';
        }

        if (element.variable && variable != null && variable != '') {
            string += that.escapeRegExp(variable);
        } else {
            if (typeof element.regex == 'function') {
                string += element.regex();
            } else {
                string += element.regex;
            }
        }

        if (capture_type == 'inner' && element.capture) {
            string += ')';
        }

    });

    if (capture_type == 'all') {
        string += ')';
    }

    // cachge stuff (again)
    this.cache.set(cacheID, new RegExp(string, this.patterns[regex_name].modifier));
    return this.cache.get(cacheID);
};
