/**
 * Created by equinox on 27.07.16.
 */

function TemplateEngine(grunt, options) {
    this.grunt = grunt;
    this.options = extend({
        removeComments: false,
        removeLinebreaks: true,
        removeFailedTemplateBlocks: true,
        dieOnError: false
    }, options);

    this.templateContent = '';

    this.fileCache = new Cache();
    this.regexBuilder = new RegexBuilder();

    this.setup();
}

TemplateEngine.prototype.setup = function() {
    var that = this;

    this.regexBuilder.register('template_blocks', [
        {
            regex: "<template\\s+file=\""
        },
        {
            regex: "[^\"]+",
            capture: true,
            variable: true
        },
        {
            regex: "\"\\s*\\/>"
        },
        {
            regex: function() {
                return (that.options.removeLinebreaks) ? "\\n?" : "";
            }
        }
    ], '');

    if (this.options.removeComments) {
        // javascript comments
        this.regexBuilder.register('javascript_comments', [
            {
                regex: "\\/\\*{2,}[^]+\\*\\/",
                capture: true
            },
            {
                regex: function() {
                    return (that.options.removeLinebreaks) ? "\\n?" : "";
                }
            }
        ], 'g');

        // html comments
        this.regexBuilder.register('html_comments', [
            {
                regex: "<!-{2,}[^+]+-->",
                capture: true
            },
            {
                regex: function() {
                    return (that.options.removeLinebreaks) ? "\\n?" : "";
                }
            }
        ], 'g');
    }

};

TemplateEngine.prototype.loadTemplate = function (file) {
    var filepath = this.grunt.template.process(file);

    if (!this.grunt.file.exists(filepath))
        throw 'File "' + filepath + '" does not exist';

    if (this.fileCache.has(filepath)) {
        throw "Loop detected: File " + filepath + " already parsed";
    }

    this.fileCache.set(filepath, this.grunt.file.read(filepath));
    return this.fileCache.get(filepath);
};

TemplateEngine.prototype.addSource = function (file) {
    try {
        this.templateContent += this.loadTemplate(file);
    } catch (err) {
        this.grunt.log.error('Error loading file ' + file + ': ' + err);
        if (this.options.dieOnError) throw(err);
        return;
    }

    this.parse();
};

TemplateEngine.prototype.parse = function () {
    var that = this,
        permanent_error = false;

    this.regexBuilder.getApplicableRegexes().forEach(function(regex_name) {

        try {
            var regex = that.regexBuilder.build(regex_name, 'inner'),
                match = regex.exec(that.templateContent);

            if (match == null) {
                return false;
            }

            var template_name = match[1],
                replace_regex = that.regexBuilder.build(regex_name, 'all', template_name);

            try {

                if (regex_name == 'template_blocks') {
                    var template_content = that.loadTemplate(template_name);
                    that.templateContent = String(that.templateContent).replace(replace_regex, template_content);
                } else {
                    that.templateContent = String(that.templateContent).replace(replace_regex, '');
                }

            } catch (load_template_error) {
                that.grunt.log.errorlns('Error loading template block from file "' + template_name + '": ' + load_template_error);

                if (that.options.removeFailedTemplateBlocks) {
                    that.grunt.log.debug('Removing template block');
                    that.templateContent = String(that.templateContent).replace(replace_regex, '');
                } else {

                }

                if (that.options.dieOnError) throw(load_template_error);

            }

        } catch (builder_error) {
            permanent_error = true;
            that.grunt.log.error('RegexBuilder error: ' + builder_error);
        }

    });

    if (permanent_error) {
        that.grunt.log.error('Permanent error, giving up');
        if (that.options.dieOnError) throw('');
        return;
    }

    try {
        var still_regex = that.regexBuilder.build('template_blocks', 'inner'),
            still_match = still_regex.exec(this.templateContent);

        if (still_match != null) {
            this.parse();
        }
    } catch (err) {
        that.grunt.log.error('RegexBuilder error: ' + err);
        if (that.options.dieOnError) throw(err);
    }
};

TemplateEngine.prototype.replaceData = function(data) {
    var that = this;

    keys(data).forEach(function(key) {
        var r = new RegExp("<%=\\s+" + key + "\\s+%>");
        that.templateContent = that.templateContent.replace(r, data[key]);
    });
};

TemplateEngine.prototype.getTemplateContent = function () {
    return this.templateContent;
};