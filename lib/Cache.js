/**
 * Created by equinox on 28.07.16.
 */

function Cache() {
    this.cache = {};
}

Cache.prototype.has = function(id) {
    return (this.cache[id] != null);
};

Cache.prototype.get = function(id) {
    return this.cache[id];
};

Cache.prototype.set = function(id, data) {
    this.cache[id] = data;
};